﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchSystem
{
    public class Search
    {
        public string[] FindInIndex(IDictionary<string, string[]> docIndex,
            IDictionary<string, string[]> invertedIndex, string expression)
        {
            var filters = ExpressionParser.Parse(expression);

            var result = filters
                .SelectMany(f => ProcessFilter(docIndex, invertedIndex, f.include, f.exclude))
                .ToArray();

            return result;
        }

        private string[] ProcessFilter(IDictionary<string, string[]> docIndex,
            IDictionary<string, string[]> invertedIndex, ICollection<string> include,
            ICollection<string> exclude)
        {
            var docsWithIncluded = invertedIndex
                .Where(kvp => include.Contains(kvp.Key.ToLower()))
                .SelectMany(kvp => kvp.Value.Where(doc =>
                    include.All(w => docIndex[doc].Any(x => string.Equals(x, w, StringComparison.InvariantCultureIgnoreCase)))))
                .Distinct()
                .ToList();
            var docsWithExcluded = invertedIndex
                .Where(kvp => exclude.Contains(kvp.Key.ToLower()))
                .SelectMany(kvp => kvp.Value)
                .Distinct()
                .ToList();

            return docsWithIncluded.Except(docsWithExcluded).ToArray();
        }
    }
}