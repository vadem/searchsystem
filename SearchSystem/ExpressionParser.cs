﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SearchSystem
{
    public static class ExpressionParser
    {
        private static readonly Regex AllowedSymbols = new Regex(@"^[а-яА-Я\d!|& ]+$", RegexOptions.Compiled);

        public static ICollection<(ICollection<string> include, ICollection<string> exclude)> Parse(string expression)
        {
            if (!AllowedSymbols.IsMatch(expression))
                return null;

            var result = expression.ToLower()
                .Split('|', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split('&', StringSplitOptions.RemoveEmptyEntries))
                .Select(GetIncludedExcludedWords)
                .ToList();

            return result;
        }

        private static (ICollection<string> include, ICollection<string> exclude) GetIncludedExcludedWords(
            string[] words)
        {
            var include = new List<string>();
            var exclude = new List<string>();

            foreach (var word in words.Select(x => x.Trim()))
            {
                var includeCurrent = GetIncludeOrExcludeWord(word);

                if (includeCurrent)
                    include.Add(word);
                else
                    exclude.Add(word.Substring(1));
            }

            return (include, exclude);
        }

        private static bool GetIncludeOrExcludeWord(string word)
        {
            if (word.StartsWith("!"))
            {
                return false;
            }

            return true;
        }
    }
}