﻿using System.Collections.Generic;
using System.Linq;

namespace SearchSystem
{
    public class Index
    {
        public IDictionary<string, string[]> GetInvertedIndex(
            IDictionary<string, string[]> docsWithWords)
        {
            return docsWithWords
                .SelectMany(doc =>
                    doc.Value.Select(w => new { Doc = doc.Key, Word = w }))
                .GroupBy(x => x.Word)
                .ToDictionary(x => x.Key, 
                    x => x.Select(y => y.Doc).Distinct().ToArray());
        }
    }
}