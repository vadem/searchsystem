﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SearchSystem
{
    class Program
    {
        private const string Url =
            "https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D0%BD%D0%B4%D0%B5%D0%BC%D0%B8%D1%8F_COVID-19";

        private const string MainOutputDirectory = @"C:\searchsystem";

        private const string OutputLinksFileName = "index.txt";

        private const string OutputTextDocsDirectory = @"C:\searchsystem\docs";

        private const string TokenFileDirectory = @"C:\searchsystem\token";

        private const string TokensFileName = "tokens.txt";

        private const string LemmasFileName = "lemmas.txt";

        private static async Task Main(string[] args)
        {
            await Task1();
            await Task2();

            Console.Write("Enter expression: ");
            await Task3(Console.ReadLine());
        }

        private static async Task Task1()
        {
            var crawler = new Crawler();
            var doc = await crawler.ExtractHtmlDocAsync(Url);
            var links = crawler.ExtractLinks(Url, doc);

            if (!Directory.Exists(MainOutputDirectory))
                Directory.CreateDirectory(MainOutputDirectory);

            using (var linkWriter = new StreamWriter($"{MainOutputDirectory}\\{OutputLinksFileName}"))
            {
                await crawler.SaveDoc(new Uri(Url), linkWriter, OutputTextDocsDirectory);
                await crawler.SaveDocs(links, linkWriter, OutputTextDocsDirectory);
            }
        }

        private static async Task Task2()
        {
            var tokenizer = new Tokenizer();
            var lemmatizer = new Lemmatizer();
            var allTokens = new HashSet<string>();

            using (var linkReader = new StreamReader($"{MainOutputDirectory}\\{OutputLinksFileName}"))
            {
                var fileNames = (await linkReader.ReadToEndAsync())
                    .Split("\n")
                    .Where(l => !string.IsNullOrWhiteSpace(l))
                    .Select(l => $"{l.Split(" ")[0]}.txt");

                foreach (var fileName in fileNames)
                {
                    var text = File.ReadAllText($"{OutputTextDocsDirectory}\\{fileName}")
                        .Split(' ');
                    var tokens = tokenizer.ExtractTokens(text);
                    allTokens.UnionWith(tokens);
                }
            }

            if (!Directory.Exists(TokenFileDirectory))
                Directory.CreateDirectory(TokenFileDirectory);

            File.WriteAllText($"{TokenFileDirectory}\\{TokensFileName}", string.Join(" ", allTokens));
            var lemmas = lemmatizer.GetLemmas(allTokens);
            File.WriteAllText($"{TokenFileDirectory}\\{LemmasFileName}", string.Join(" ", lemmas));
        }

        private static async Task Task3(string expression)
        {
            var index = new Index();
            var search = new Search();
            var fileNames = File.ReadAllText($"{MainOutputDirectory}\\{OutputLinksFileName}")
                .Split('\n', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => $"{x.Split(' ')[0]}.txt")
                .ToArray();
            var docIndex = fileNames
                .ToDictionary(fn => fn, fn => File.ReadAllText($"{OutputTextDocsDirectory}\\{fn}").Split(' '));
            var invertedIndex = index.GetInvertedIndex(docIndex);

            Console.WriteLine("Result (task3): ");
            Console.WriteLine(string.Join("\n", search.FindInIndex(docIndex, invertedIndex, expression)));
        }
    }
}