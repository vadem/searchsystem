﻿using System.Collections.Generic;

namespace SearchSystem
{
    public class Tokenizer
    {
        public ISet<string> ExtractTokens(IEnumerable<string> text)
        {
            var result = new HashSet<string>();

            foreach (var word in text)
            {
                if (result.Contains(word.ToLower()))
                    continue;

                result.Add(word.ToLower());
            }

            return result;
        }
    }
}