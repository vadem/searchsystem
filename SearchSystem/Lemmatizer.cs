﻿using System.Collections.Generic;
using System.Linq;
using DeepMorphy;

namespace SearchSystem
{
    public class Lemmatizer
    {
        public Lemmatizer()
        {
            Analyzer = new MorphAnalyzer(true);
        }

        private MorphAnalyzer Analyzer { get; }

        public string GetLemma(string word)
        {
            return Analyzer.Parse(new[] { word })
                .FirstOrDefault()?.BestTag?.Lemma;
        }

        public ISet<string> GetLemmas(ISet<string> tokens)
        {
            var result = new HashSet<string>();

            foreach (var token in tokens)
            {
                var lemma = GetLemma(token);

                if (lemma != null && !result.Contains(lemma))
                    result.Add(lemma);
            }

            return result;
        }
    }
}