﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;

namespace SearchSystem
{
    public class Crawler
    {
        private const int MinWordsCountPerPage = 1000;

        private const int MinLinkTotalCount = 100;

        private static readonly string NodesToSelect
            = string.Join("|", new[] { "a", "p", "h1", "h2", "h3", "h4", "h5", "h6", "span", "br", "ul", "li" }
                .Select(tag => $"//{tag}"));

        private static readonly char[] SplitChars = { ' ', '\n', '\r', '\t' };

        private static readonly char[] TrimChars =
            { '.', ',', '!', '?', ':', ';', '\'', '"', '(', ')', '-', '—', '<', '>', '/', '|', '{', '}', '[', ']' };

        private HtmlWeb Web { get; }

        private int _current;
        private ICollection<Uri> _links = new List<Uri>();

        public Crawler()
        {
            Web = new HtmlWeb
            {
                OverrideEncoding = Encoding.UTF8
            };
        }

        public async Task<HtmlDocument> ExtractHtmlDocAsync(string url) => await Web.LoadFromWebAsync(url);

        public IEnumerable<string> ExtractPageText(HtmlDocument document)
        {
            var filteredTagInnerTexts = document.DocumentNode
                .SelectNodes(NodesToSelect)
                ?.Select(x => HttpUtility.HtmlDecode(x.InnerText))
                .Where(x => x != null)
                .SelectMany(x => x.Trim().Split(SplitChars, StringSplitOptions.RemoveEmptyEntries))
                .Select(x => x.Trim(TrimChars))
                .Where(x => !string.IsNullOrWhiteSpace(x) && Regex.IsMatch(x, @"^[А-Яа-я]+$"));

            return filteredTagInnerTexts;
        }

        public async Task SaveDocs(ICollection<Uri> uris, StreamWriter linkWriter,
            string outputDirectory)
        {
            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            foreach (var uri in uris)
            {
                if (_links.Contains(uri))
                    continue;

                _links.Add(uri);

                await SaveDoc(uri, linkWriter, outputDirectory);

                if (_current >= MinLinkTotalCount)
                    return;
            }

            if (_current >= MinLinkTotalCount)
                return;

            foreach (var uri in uris)
            {
                var doc = await ExtractHtmlDocAsync(uri.AbsoluteUri);
                var links = ExtractLinks(uri.AbsoluteUri, doc);

                await SaveDocs(links, linkWriter, outputDirectory);
            }
        }

        public async Task SaveDoc(Uri uri, StreamWriter linkWriter, string outputDirectory)
        {
            var doc = await ExtractHtmlDocAsync(uri.AbsoluteUri);
            var text = ExtractPageText(doc);

            if (text == null)
                return;

            var textCollection = text.ToList();

            if (textCollection.Count < MinWordsCountPerPage)
                return;

            linkWriter.WriteLine($"{++_current} {uri.AbsoluteUri}");

            using (var sw = new StreamWriter($"{outputDirectory}\\{_current}.txt"))
            {
                sw.WriteLine(string.Join(" ", textCollection));
            }
        }

        public ICollection<Uri> ExtractLinks(string url, HtmlDocument document)
        {
            var baseUri = new Uri(url);
            var filteredLinks = document.DocumentNode
                .SelectNodes("//a")
                ?.Select(x => HttpUtility.UrlDecode(x.Attributes.FirstOrDefault(a => a.Name == "href")?.Value))
                .Where(x => x != null)
                .Select(x => GetAbsoluteUri(baseUri,
                    Uri.TryCreate(x, x.StartsWith("/") ? UriKind.Relative : UriKind.Absolute, out var uri)
                        ? uri
                        : null))
                .Where(x => x != null)
                .Distinct()
                .ToList();

            return filteredLinks;
        }

        private static Uri GetAbsoluteUri(Uri baseUri, Uri relative)
        {
            if (baseUri == null || relative == null)
                return null;

            if (relative.IsAbsoluteUri)
                return relative;

            return Uri.TryCreate(new Uri($"{baseUri.Scheme}://{baseUri.Authority}", UriKind.Absolute), relative,
                out var uri)
                ? uri
                : null;
        }
    }
}